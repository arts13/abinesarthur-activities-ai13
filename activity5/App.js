import React from 'react';
import { Text, View,TouchableHighlight,} from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state= {
      primaryDisplay: '0',
      clear: true,
    }

    this.changeMod = this.changeMod.bind(this);
    this.clearScreen = this.clearScreen.bind(this);
  }

  
    operationButtons(value, functionCall = null) {
      return(
        <View style= {styles.calculatorCells}>
          <TouchableHighlight
            style={styles.primaryButtons}
            onPress={() =>  functionCall ? functionCall() : this.onClickHandler(value)}
          >
            <Text style={styles.primaryButtonsText}> {value} </Text>
          </TouchableHighlight>
        </View>
      );
    }
  
    onClickHandler(value) {
      const { primaryDisplay, clear } = this.state;
      let newValue = clear ? `${value}` : primaryDisplay.concat('' + value);
      this.setState({
        primaryDisplay: newValue,
        clear: false,
      })
    }
  
    getResult() {
      const { primaryDisplay } = this.state;
      try {
        let submitString = primaryDisplay.replace('x', '*').replace('÷', '/');
        let result = `${eval(submitString)}`;
       
        this.setState({
          primaryDisplay: result,
        });
      } catch(e) {
          console.log(e);
      }
    }
  
    clearScreen() {
      this.setState({
        primaryDisplay: '0',
        clear: true,
      })
    }
  
    backPress() {
      const { clear, primaryDisplay } = this.state;
      let displayArray =  primaryDisplay.split(' ');
  
      if (displayArray.length === 1) {
        const result = clear
        ? `${primaryDisplay}`
        : primaryDisplay.substr(0, primaryDisplay.length - 1);
        this.setState({
          primaryDisplay: result.length === 0 ? '0' : result,
          clear: result.length === 0,
        })
      } else {
        displayArray.pop();
        const result = displayArray.join(' ');
        this.setState({
          primaryDisplay: result,
        })
      }
    }
  
    numbersButtons(value) {
      return(
        <View style= {styles.calculatorCells}>
          <TouchableHighlight
            style={styles.secondaryButtons}
            onPress={() => this.onClickHandler(value)}
          >
            <Text style={styles.secondaryButtonsText}>{value}</Text>
          </TouchableHighlight>
        </View>
      )
    }
  
    changeMod() {
      const { primaryDisplay } = this.state;
      const negMod = '-(';
      let res = '';
      if (primaryDisplay.includes(negMod)) {
        res = primaryDisplay.replace('-(', '').replace(')', '');
      } else {
        res = primaryDisplay === '0' ? primaryDisplay : `-(${primaryDisplay})`;
      }
  
      this.setState({
        primaryDisplay: res,
      })
    }

    equalsButton() {
      return(
        <View style= {styles.calculatorCellsDouble}>
          <TouchableHighlight
            style={[styles.primaryButtons, {backgroundColor: '#F27030'}]}
            onPress={() => this.getResult()}
          >
            <Text style={styles.primaryButtonsText}> = </Text>
          </TouchableHighlight>
        </View>
      );
    }
  
    firstRow() {
      return(
        <View style={styles.row_layout}>
          {this.operationButtons('C', this.clearScreen)}
          {this.operationButtons('+/-', this.changeMod)}
          {this.operationButtons('%')}
          {this.operationButtons('÷')}
        </View>
  
      );
    }
  
    secondRow() {
      return(
        <View style={styles.row_layout}>
          {this.numbersButtons(7)}
          {this.numbersButtons(8)}
          {this.numbersButtons(9)}
          {this.operationButtons('x')}
        </View>
      );
    }
  
  
    thirdRow() {
      return(
        <View style={styles.row_layout}>
          {this.numbersButtons(4)}
          {this.numbersButtons(5)}
          {this.numbersButtons(6)}
          {this.operationButtons('-')}
        </View>
      );
    }
  
    fourthRow() {
      return(
        <View style={styles.row_layout}>
          {this.numbersButtons(1)}
          {this.numbersButtons(2)}
          {this.numbersButtons(3)}
          {this.operationButtons('+')}
      </View>
      );
    }
  
    fifthRow() {
      return(
        <View style={styles.row_layout}>
          {this.numbersButtons(0)}
          {this.numbersButtons('.')}
          {this.equalsButton()}
      </View>
      );
    }

    render() {
      const { primaryDisplay } = this.state;
      return(
        <View  style={styles.appContainer}>
          <View style={styles.header}>
            <View style={styles.headerContainer}>
              <View style={{ width: '100%', flexDirection: 'row-reverse', marginTop: 30 }}>
                <Text style={{ fontSize: 20, color: '#848A8F' }}> </Text>
              </View>
              <View style={{ flexDirection: 'row-reverse' , width: '100%', marginTop: 25}}>
                <Text style={{ fontSize: 28, color: 'white' }}> {primaryDisplay} </Text>
              </View>
  
              <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '50%'}}>
                  <TouchableHighlight
                    onPress={() => this.toggleModal()}
                  >
                    <View style={{ width: 0, height: 0 }}></View>
                  </TouchableHighlight>
                </View>
                <View style={{ width: '50%', flexDirection: 'row-reverse' }}>
                  <TouchableHighlight
                    onPress={() => this.backPress()}
                  >
                    <View style={{ width: 30, height: 18 }}></View>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.body}>
            {this.firstRow()}
            {this.secondRow()}
            {this.thirdRow()}
            {this.fourthRow()}
            {this.fifthRow()}
          </View>
  
        </View>
      );
  }
}


const styles = {
  appContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#06121B',
  },
  header: {
    height: '26%',
    marginTop: 25,
  },

  headerContainer: {
    width: '95%',
    height: '100%',
    alignSelf: 'center',
  },

  body: {
    width: '100%',
    height: '71%',
  },
  calculatorCells: {
    width: `${100/4}%`,
    height: 90,
  },
  calculatorCellsDouble: {
    width: `${100/2}%`,
    height: 90,
  },
  row_layout: {
   flexDirection: 'row',
   marginTop: 3,

  },
  primaryButtons: {
    width: '98%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00fffb',
    borderRadius:50
  },
  
  primaryButtonsText: {
    color: 'black',
    fontSize: 28,
    fontWeight: 'bold',
  },
  secondaryButtons :{
    width: '98%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3F4F5B'
  },
  secondaryButtonsText: {
    fontSize: 28,
    color: 'white',
  },
};