import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <input id="arts-text"  type="text"></input>
      <button onClick={mainFunction}>Button</button>
    </View>
  );
}

function mainFunction() {
  alert(document.getElementById("arts-text").value);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
