import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './Screens/Home'
import OneScreen from './Screens/OneScreen';
import TwoScreen from './Screens/TwoScreen';
import ThreeScreen from './Screens/ThreeScreen';
import FourScreen from './Screens/FourScreen';
import FiveScreen from './Screens/FiveScreen';

export default function App() {
  return (
    <NavigationContainer >
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="OneScreen" component={OneScreen} options={{headerShown: false}} />
        <Stack.Screen name="TwoScreen" component={TwoScreen} options={{headerShown: false}} />
        <Stack.Screen name="ThreeScreen" component={ThreeScreen} options={{headerShown: false}} />
        <Stack.Screen name="FourScreen" component={FourScreen} options={{headerShown: false}} />
        <Stack.Screen name="FiveScreen" component={FiveScreen} options={{headerShown: false}} />
        <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}} />
      </Stack.Navigator>
    </NavigationContainer>
  );  
}
const Stack = createNativeStackNavigator();