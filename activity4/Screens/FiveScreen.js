import * as React from 'react';
import { StyleSheet ,View , Text , TouchableOpacity , Image} from 'react-native';


const FiveScreen = ({ navigation }) => {
    return (
      <View style={styles.lists}>
    
      <Image style={styles.img} source={require('../assets/ITimg/undraw_Target_re_fi8j.png')}></Image>
      <View style={styles.txt_container}>
          <Text style={styles.txt}>To become a Developer and create advance websites and applications with high security.</Text>
      </View>
      <View style={{flex: 1,justifyContent: 'flex-end'}}>
      <TouchableOpacity style={styles.btnContainer}
        onPress={() =>
          navigation.navigate('Home')}>
        <Text style={styles.btnText}>Home</Text>
      </TouchableOpacity>

      </View>
      </View>
    );
  };

  const styles = StyleSheet.create({
    lists: {
      flex: 1,
      padding:10,
      paddingTop: 80,
      justifyContent: 'center',
    },
    txt_container:{
      width: '100%', 
      marginTop: 20,
      borderRadius: 25,
      borderWidth: 2, 
      padding: 10,
      alignItems: "center",
      borderColor: "black",
    },
    txt: {
      fontSize: 30,
      color:'black'
    },
    btnText: {
      fontSize: 20,
      color: 'white',
      fontWeight: 'bold'
    },
    btnContainer:  {
      alignItems: "center",
      backgroundColor: 'blue',
      height: 50,
      borderRadius: 30,
      paddingVertical: 10,
    },
    img: {
      width: '100%', 
      height:350,
      borderRadius: 20,
      borderWidth: 3, 
      borderColor: "black"
    }
  });
export default FiveScreen;