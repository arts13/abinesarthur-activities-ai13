import * as React from 'react';
import { StyleSheet ,View , Text , TouchableOpacity , Image} from 'react-native';


const FourScreen = ({ navigation }) => {
    return (
      <View style={styles.lists}>
       <View style={styles.img}> 
        <Image style={{resizeMode: 'center',width: '100%', height:350}}source={require('../assets/ITimg/undraw_progressive_app_m9ms.png')}></Image>
       </View>
       <View style={styles.txt_container}>
            <Text style={styles.txt}>To learn how to develop websites and applications.</Text>
            </View>
      <View style={{flex: 1,justifyContent: 'flex-end'}}>
      <TouchableOpacity style={styles.btnContainer}
        onPress={() =>
          navigation.navigate('FiveScreen')}>
        <Text style={styles.btnText}>Continue</Text>
      </TouchableOpacity>

      </View>
      </View>
    );
  };
    
  const styles = StyleSheet.create({
    lists: {
      flex: 1,
      padding:10,
      paddingTop: 80,
      justifyContent: 'center',
    },
    txt_container:{
      width: '100%', 
      marginTop: 20,
      borderRadius: 25,
      borderWidth: 2, 
      padding: 10,
      alignItems: "center",
      borderColor: "black",
    },
    txt: {
      fontSize: 30,
      color:'black'
    },

    container: {
      flex: 1,
      justifyContent: "center",
      paddingHorizontal: 10
    },
    btnText: {
      fontSize: 20,
      color: 'white',
      fontWeight: 'bold'
    },
    btnContainer:  {
      alignItems: "center",
      backgroundColor: 'blue',
      height: 50,
      borderRadius: 30,
      paddingVertical: 10,
    },
    img: {
      width: '100%', 
      height:350,
      borderRadius: 20,
      borderWidth: 3, 
      borderColor: "black"
    }
  });
export default FourScreen;