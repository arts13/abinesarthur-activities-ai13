import * as React from 'react';
import {Text, Image, View , StatusBar , StyleSheet, SafeAreaView, ScrollView} from 'react-native';

export default HomeScreen = () => {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{alignItems: "center"}}>
        <Text style={styles.SBarText}>Welcome to home</Text>
        </View>
        <ScrollView style={styles.scrollView}>
          <View style={{alignItems: "center"}}>
            <Text style={styles.txt}>First Screen</Text>
            <Image style={styles.img} source={require('../screenshots/s1.jpg')}></Image>
            <Text style={styles.txt}>Second Screen</Text>
            <Image style={styles.img} source={require('../screenshots/s2.jpg')}></Image>
            <Text style={styles.txt}>Third Screen</Text>
            <Image style={styles.img} source={require('../screenshots/s3.jpg')}></Image>
            <Text style={styles.txt}>Fourth Screen</Text>
            <Image style={styles.img} source={require('../screenshots/s4.jpg')}></Image>
            <Text style={styles.txt}>Fifth Screen</Text>
            <Image style={styles.img} source={require('../screenshots/s5.jpg')}></Image>
            <Text style={styles.txt}>Home Screen</Text>
            <Image style={styles.img} source={require('../screenshots/s6.jpg')}></Image>
          </View>
        </ScrollView>
    </SafeAreaView>
    );
  };
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: StatusBar.currentHeight,
    },
    scrollView: {
      margin: 5,
    },
    SBarText:{
      alignItems: "center",
      color:'black',
      fontSize: 30
    },
    txt: {
      marginTop: 20,
      alignItems: "center",
      color:'black',
      fontSize: 30,
      textShadowRadius:20,
    },
    img: {
      borderColor: "blue",
      borderRadius: 20,
      marginTop: 10,
      borderWidth: 3, 
      width: '100%', 
      height: 700
    }
  });
