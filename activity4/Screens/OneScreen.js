import * as React from 'react';
import {View , Text , Image , TouchableOpacity, StyleSheet} from 'react-native';


const OneScreen = ({ navigation }) => {
    return (
      <View style={styles.lists}>
        <Image style={styles.img} source={require('../assets/ITimg/undraw_Developer_activity_re_39tg.png')}></Image>
        <View style={styles.txt_container}>
          <Text style={styles.txt}>
            I choosed IT because I enjoy coding and creating applications.
          </Text>
        </View>
        <View style={{flex: 1,justifyContent: 'flex-end'}}>
          <TouchableOpacity style={styles.btnContainer}
            onPress={() =>
            navigation.navigate('TwoScreen')}>
            <Text style={styles.btnText}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
    
  const styles = StyleSheet.create({
    lists: {
      flex: 1,
      padding:10,
      paddingTop: 80,
      justifyContent: 'center',
    },
    txt_container:{
      width: '100%', 
      marginTop: 20,
      borderRadius: 25,
      borderWidth: 2, 
      padding: 10,
      alignItems: "center",
      borderColor: "black",
    },
    txt: {
      fontSize: 30,
      color:'black'
    },
    btnText: {
      fontSize: 20,
      color: 'white',
      fontWeight: 'bold'
    },
    btnContainer:  {
      alignItems: "center",
      backgroundColor: 'blue',
      height: 50,
      borderRadius: 30,
      paddingVertical: 10,
    },
    img: {
      width: '100%', 
      height:350,
      borderRadius: 20,
      borderWidth: 3, 
      borderColor: "black"
    }
  });
export default OneScreen;