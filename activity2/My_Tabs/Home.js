import React, { useState } from 'react';
import { Text, View, TextInput, ScrollView, Keyboard, TouchableOpacity, StyleSheet} from 'react-native';
import Task from '../arts/designs';

export default function HomeScreen() {
    const [task, my_task] = useState();
    const [taskItems, task_item] = useState([]);
    const handleAddTask = () => {
        Keyboard.dismiss();
        task_item([...taskItems, task])
        my_task("");
    }
    const task_complete = (index) => {
        let itemsCopy = [...taskItems];
        itemsCopy.splice(index, 1);
        task_item(itemsCopy)
    }
    return (
        <View style={styles.container}>
            <TextInput style={styles.arts_text} placeholder={'Input text'} value={task} onChangeText={text => my_task(text)} />
            <View style={styles.arts_btn}>
                <TouchableOpacity onPress={() => handleAddTask()}>
                    <Text style={styles.add_btn}>ADD TASK</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scroll_me} contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
                <View style={styles.items}>
                    {
                        taskItems.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => task_complete(index)}>
                                    <Task text={item} />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    );
  }

const styles = StyleSheet.create({
    container: {
      marginTop: 60,
      flex: 1,
      position: 'relative',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center',
    },
  
    add_btn: {
      paddingHorizontal: 14,
      backgroundColor: '#002c85',
      textAlign: 'center',
      color: '#fff',
      fontWeight: 'bold',
    },
  
    arts_btn: {
      marginTop: 20,
      paddingVertical: 10,
      paddingHorizontal: 14,
      backgroundColor: '#002c85',
      borderColor: '#db0000',
      borderWidth: 2,
      borderRadius: 20,
      width: '90%',
    },
  
    arts_text: {
      paddingVertical: 14,
      paddingHorizontal: 14,
      backgroundColor: '#FFF',
      borderColor: '#000',
      borderWidth: 2,
      borderRadius: 6,
      width: '90%',
    },
  
    scroll_me: {
      marginTop: 20,
      width: '90%',
    },
  });