import React from "react";
import {StyleSheet, View, Text} from "react-native";
//import { CheckBox, TouchableOpacity } from "react-native-web";

const Abines_Task = (props) => {
    return (
        <View style={styles.lists}>
            <View style={styles.added_list}>
                <View style={styles.circle}></View>
                <Text style={styles.added_text}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    lists: {
        backgroundColor: '#00f7ff',
        paddingLeft: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 20,
        borderWidth: 2,
        borderRadius: 10,
    },
    circle: {
        width: 30,
        height: 30,
        marginRight: 15,
        backgroundColor: '#faf602',
        borderWidth: 3,
        borderColor: '#666300',
        borderRadius: 60,
    },
    added_list: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    added_text: {
        color: '#000000',
        maxWidth: '85%',
        fontWeight: 'bold',
        fontSize: 24,
    },
});
export default Abines_Task;